package net.sxbang.service;

import net.sxbang.dao.UserDAO;
import net.sxbang.dao.UserDAOImpl;
import net.sxbang.model.User;

public class UserServiceImpl implements UserService {

	private UserDAO userDAO = new UserDAOImpl();
	
	@Override
	public String register(String username, String password) {
		System.out.println(this.getClass().getName() + "register");
		User user = userDAO.findByUsername(username);
		if (user == null) {
			user = new User();
			user.setPassword(username);
			user.setPassword(password);
			userDAO.save(user);
			return "注册成功";
		}
		return "已存在用户";
	}

}
