package net.sxbang.dao;

import net.sxbang.model.User;

public interface UserDAO {

	User findByUsername(String username);
	
	void save(User user);
	
}
